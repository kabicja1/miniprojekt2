from flask import Flask, render_template, url_for, jsonify, request
from routes import register_routes

app = Flask(__name__)
register_routes(app)

name = "John"
    
measured_values = [
        {"id": 1,"timestamp": "02/Apr/2024 12:00:00", "temp": 25},
        {"id": 2,"timestamp": "02/Apr/2024 11:30:00", "temp": 24},
        {"id": 3,"timestamp": "02/Apr/2024 11:00:00", "temp": 26},
        {"id": 4,"timestamp": "02/Apr/2024 10:30:00", "temp": 24},
        {"id": 5,"timestamp": "02/Apr/2024 10:00:00", "temp": 25},
        {"id": 6,"timestamp": "02/Apr/2024 9:30:00", "temp": 18},
        {"id": 7,"timestamp": "02/Apr/2024 9:00:00", "temp": 13},
        {"id": 8,"timestamp": "02/Apr/2024 8:30:00", "temp": 12},
        {"id": 9,"timestamp": "02/Apr/2024 8:00:00", "temp": 12},
        {"id": 10,"timestamp":"02/Apr/2024 7:30:00", "temp": 10},
    ]
    

def generate_task_id():
    return min(task['id'] for task in measured_values) -1 if measured_values else 1


if __name__ == "__main__":  
    app.run(host='0.0.0.0', port=5000, debug=True, use_reloader=False)

