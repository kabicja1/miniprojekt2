from flask import Blueprint, render_template

web_routes_bp = Blueprint('web_routes', __name__, static_folder='static', static_url_path='/static')

@web_routes_bp.route('/')   
def dashboard():  
    from app import measured_values, name
    return render_template('dashboard.html', name = name, measured_values=measured_values) 

@web_routes_bp.route('/login')   
def login():  
    
    from app import name
    return render_template('login.html', name = name)  

@web_routes_bp.route('/register')   
def register():  

    from app import name
    return render_template('register.html', name = name)  