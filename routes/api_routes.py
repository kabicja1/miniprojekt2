from flask import Blueprint, jsonify, request

api_routes_bp = Blueprint('api_routes', __name__)


@api_routes_bp.route('/api/tasks', methods=['GET'])
def get_tasks():
    from app import measured_values
    return jsonify(measured_values)

@api_routes_bp.route('/api/tasks/<int:task_id>', methods=['GET'])
def get_task(task_id):
    from app import measured_values
    min_id = min(task['id'] for task in measured_values)
    task_range = [task for task in measured_values if task['id'] <= (min_id + task_id)-1]
    if task_range:
        return jsonify(task_range)
    else:
        return jsonify({"message": "Task not found"}), 404

@api_routes_bp.route('/api/tasks', methods=['POST'])
def create_task():

    from app import measured_values, generate_task_id
    new_task = request.json
    new_task['id'] = generate_task_id()
    measured_values.insert(0,new_task)
    return jsonify(new_task), 201

@api_routes_bp.route('/api/delete_last/<int:num_to_delete>', methods=['DELETE'])
def delete_last(num_to_delete):

    from app import measured_values
    if num_to_delete > 0:
        max_id = max(task['id'] for task in measured_values)
        measured_values[:] = [task for task in measured_values if task['id'] <= max_id - num_to_delete]
        return jsonify(measured_values)
    else:
        return jsonify({"message": "Invalid number of items to delete"}), 400

